## LProgram

This program was created when I was a student (2017), simply to list all the programs. It's a basic task manager. You will need to use UAC to open this program. I Created in 2017.

### Features

- Login Screen (hard-code)
- Upon opening, the program will terminate xclass program.
- It lists all the programs on your system.

That all!

![login](./static/Program-Login.png)

![login](./static/Program-main.gif)

## Compilation

This program is built using Visual Studio IDE. Here are the steps to compile it:

1. Download [Visual Studio IDE](https://visualstudio.microsoft.com/).
2. Install .NET desktop development workloads via the Visual Studio Installer. By default, it includes C#, VB.NET, F#, and more.
3. Clone this repository.
4. Open the .sln file.
5. Click "Start" at the top to initiate the compilation process. (Please note that this will require UAC permissions to restart the VS IDE.)

Once compiled, you can find the executable in your root directory -> bin -> Debug/Release directory.